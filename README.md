# TRI-ANGLE

## Compilation et interface

Ce projet est réalisé avec QT et n'a été testé que sur cette plateforme avec Linux.
L'interface se compose d'une fenêtre OpenGL et d'un bouton pour stopper l'application.

Le programme nécessite un fichier OFF pour fonctionner. Les sommets sont obligatoires mais pas les triangles.
Si le fichier contient des triangles alors on applique cette triangulation.
Sinon utilise la triangulation de Delaunay pour la créer.

Nous n'avons pas réussi à implémenter le changement de fichier avec l'interface car l'affichage ne changeait pas après chargement du nouveau fichier.
Le path pour le fichier OFF est donc codé en dur dans le fichier `gldisplaywidget.cpp` à la ligne 10.

Nous avons placé deux fichiers OFF dans un répertoire `data` pour tester l'application.
`hexagone.off` permet de mettre en avant le calcul du laplacien.
`triangle.off` quant à lui montre à la fois la triangulation de Delaunay et le graphe de Voronoï.
Nous n'avons pas inclu d'autres fichiers car ils ne fonctionnent à terme plus sur notre projet (pour de nombreuses raisons).

## Itérateurs et circulateurs

Les itérateurs sur les faces et les sommets fonctionnent sans problème.
Les circulateurs de faces et de sommets fonctionnent aussi, à condition que l'on ne circule jamais sur une face ou un sommet non défini.
L'opérateur ++ ne fonctionnerait pas et le programme renvoie une segmentation fault.
Il s'agit d'une source de blocage dans beaucoup d'autres parties du projet, que ce soit pour le test sur des maillages non fermés ou pour les calculs qui s'en servent.

## Laplacien

Le calcul du laplacien est fonctionnel si l'on respecte la condition évoquée pour les circulateurs.
Pour chaque sommet est attribué un tableau donnant la courbure moyenne sur les axes X, Y et Z.
Pour afficher cette courbure, nous l'utilisons comme poids pour la couleur de chaque sommet.
On associe ainsi les couleurs RGB aux axes XYZ.

Exemple de maillage courbé :

![Laplacien](./screenshots/laplacienApp.png)


## Triangulation de Delaunay

Pour faire la triangulation de Delaunay, nous avons utilisé un algorithme incrémental basé sur celui de Lawson.
Pour chaque point du maillage, on l'insère dans notre triangulation puis on regarde les arrêtes à flip.
Si l'on est dans un triangle alors on va utiliser le prédicat `checkInCircle` pour regarder les trois arrêtes appartenant à ce triangle.
Dès lors qu'un point se trouve dans le cercle circonscrit on va marquer l'arrête.
Quand on a fait le tour de ces arrêtes on flip toutes celles qui sont marquées.
Les calculs pour les prédicats d'orientations, de `checkInTriangle` et `checkInCircle` sont fait en double.

Exemple de fonctionnement du split et du flip :

![Split first step](./screenshots/split1.png)
![Split second step](./screenshots/split2.png)
![Flip step](./screenshots/split3.png)

Le plus gros problème que nous avons rencontré concernent l'insertion en dehors de l'enveloppe convexe.
Nous avons essayé de la faire à l'aide d'un point infini, tout semble marcher sur le papier mais nous n'arrivons pas à insérer plus d'un point en dehors de l'enveloppe.
La fonction pour le faire est donc implémentée mais nous avons dû la remplacer par une bounding box avec un tétraèdre.
Cependant ce code ne marche pas non plus parfaitement et donc beaucoup des fichiers OFF que nous avions ne fonctionnent plus.

Exemple de maillage triangulisé :

![Delaunay](./screenshots/flipSplitApp.png)
![Schéma](./screenshots/FlipSplit.png)


Sur le schéma, les arrêtes vertes sont les arrêtes ajoutées et les arrêtes rouges sont celles supprimées.
:warning: Sur le screenshot de l'application on a l'impression que les arrêtes supprimées sont toujours présentes, il s'agit en fait des arrêtes menants au point infini, il ne faut donc pas les prendre en compte.

## Voronoï

Le calcul des centres des cercles circonscrits fonctionne en 2D comme en 3D.
Nous nous sommes servis des coordonnées barycentriques pour calculer les coordonnées du centre.
À chaque face est associée son centre qui correspond à son dual.
Pour l'affichage, nous circulons pour chaque sommet sur ses faces adjacentes pour relier les centres de Voronoï entre eux.

Exemple de triangulation avec son dual :

![Voronoï](./screenshots/voronoi1.png)
![Voronoï Zooom](./screenshots/voronoi2.png)
