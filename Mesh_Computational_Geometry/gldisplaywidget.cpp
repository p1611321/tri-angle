#include "gldisplaywidget.h"
#ifdef __APPLE__
    #include <glu.h>
#else
    #include <GL/glu.h>
#endif

#include "QDebug"

enum drawMethods {PLAIN = 1, WIREFRAME, LAPLACIAN, VORONOI, CRUST};

GLDisplayWidget::GLDisplayWidget(QWidget *parent) : QGLWidget(parent), _geomWorld("../Mesh_Computational_Geometry/data/cube.off")
{
    // Update the scene
    connect( &_timer, SIGNAL(timeout()), this, SLOT(updateGL()));
    _timer.start(16); // Starts or restarts the timer with a timeout interval of 16 milliseconds.
}

void GLDisplayWidget::initializeGL()
{
    // background color
    glClearColor(0.2f, 0.2f, 0.2f, 1.f);

    // Shader
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
}

void GLDisplayWidget::paintGL(){

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Center the camera
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0,0,5,  0,0,0,   0,1,0); //gluLookAt(eye, center, up)  //deprecated
                                       // Returns a 4x4 matrix that transforms world coordinates to eye coordinates.
    // Translation
    glTranslated(_X, _Y, _Z);

    // Rotation
    glRotatef(_angle, 1.0f, 1.0f, 0.0f);

    // Color for your _geomWorld
    glColor3f(0, 1 ,0);

    // Draw function
    _geomWorld.draw();
}

void GLDisplayWidget::resizeGL(int width, int height){

    glViewport(0, 0, width, height); //Viewport in the world window
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);

    updateGL();
}

// - - - - - - - - - - - - Mouse Management  - - - - - - - - - - - - - - - -
// When you click, the position of your mouse is saved
void GLDisplayWidget::mousePressEvent(QMouseEvent *event)
{
    if( event != NULL )
        _lastPosMouse = event->pos();
}

// Mouse movement management
void GLDisplayWidget::mouseMoveEvent(QMouseEvent *event) {
    if (event != NULL) {
        const float dx = event->x() - this->_lastPosMouse.x();
        const float dy = event->y() - this->_lastPosMouse.y();
        const Qt::MouseButtons& buttons = event->buttons();
        if (buttons.testFlag(Qt::MidButton) || buttons.testFlag(Qt::MiddleButton)) {
            this->_X += dx * 0.01;
            this->_Y -= dy * 0.01;
            this->_lastPosMouse = event->pos();
        }
        if (buttons.testFlag(Qt::LeftButton)) {
            this->_angle += (dx + dy) * 0.05;
        }
        updateGL();
    }
}

// Mouse Management for the zoom
void GLDisplayWidget::wheelEvent(QWheelEvent *event) {
    const QPoint numDegrees = event->angleDelta();
    if (!numDegrees.isNull()) {
        this->_Z += numDegrees.y() / 120.0;
    }
}

void GLDisplayWidget::on_Queen_Released() {
    _geomWorld.dm = PLAIN;
    _geomWorld.loadFile("../Mesh_Computational_Geometry/data/queen.off");
}

void GLDisplayWidget::on_Cube_Released() {
    _geomWorld.dm = WIREFRAME;
    _geomWorld.loadFile("../Mesh_Computational_Geometry/data/cube.off");
}

void GLDisplayWidget::on_Hexagon_Released() {
    _geomWorld.dm = LAPLACIAN;
    _geomWorld.loadFile("../Mesh_Computational_Geometry/data/hexagon.off");
}

void GLDisplayWidget::on_Diamond_Released() {
    _geomWorld.dm = VORONOI;
    _geomWorld.loadFile("../Mesh_Computational_Geometry/data/diamond.off");
}

void GLDisplayWidget::on_Bunny_Released() {
    _geomWorld.dm = CRUST;
    _geomWorld.loadFile("../Mesh_Computational_Geometry/data/bunny.off");
}
