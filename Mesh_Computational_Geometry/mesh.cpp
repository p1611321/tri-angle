#include "mesh.h"

// Triangulation functions
void Mesh::faceSplit(int triangleIndex, int vertexIndex) {
    long long lastFaceId = this->faces.size() - 1;

    // Faces needed to be created or changed
    Face *currentFace = this->faces[triangleIndex];
    Face *newFace1 = new Face(vertexIndex, currentFace->idVertices[1], currentFace->idVertices[2]);
    Face *newFace2 = new Face(currentFace->idVertices[0], vertexIndex, currentFace->idVertices[2]);

    // Assign new adjacent faces
    newFace1->idAdjFaces[0] = currentFace->idAdjFaces[0];
    newFace1->idAdjFaces[1] = lastFaceId + 2;
    newFace1->idAdjFaces[2] = triangleIndex;

    newFace2->idAdjFaces[0] = lastFaceId + 1;
    newFace2->idAdjFaces[1] = currentFace->idAdjFaces[1];
    newFace2->idAdjFaces[2] = triangleIndex;

    int index = -1;
    if (currentFace->idAdjFaces[1] != -1) {
        if (newFace1->idVertices[2] == faces[currentFace->idAdjFaces[1]]->idVertices[0]) {
            index = 1;
        } else if (newFace1->idVertices[2] == faces[currentFace->idAdjFaces[1]]->idVertices[1]) {
            index = 2;
        } else if (newFace1->idVertices[2] == faces[currentFace->idAdjFaces[1]]->idVertices[2]) {
            index = 0;
        }

        if (index != -1)
            this->faces[currentFace->idAdjFaces[1]]->idAdjFaces[index] = lastFaceId + 2;
    }

    if (currentFace->idAdjFaces[0] != -1) {
        if (newFace1->idVertices[1] == faces[currentFace->idAdjFaces[0]]->idVertices[0]) {
            index = 1;
        } else if (newFace1->idVertices[1] == faces[currentFace->idAdjFaces[0]]->idVertices[1]) {
            index = 2;
        } else if (newFace1->idVertices[1] == faces[currentFace->idAdjFaces[0]]->idVertices[2]) {
            index = 0;
        }

        this->faces[currentFace->idAdjFaces[0]]->idAdjFaces[index] = lastFaceId + 1;
    }

    currentFace->idAdjFaces[0] = lastFaceId + 1;
    currentFace->idAdjFaces[1] = lastFaceId + 2;

    // Update vertices
    if (this->vertices[currentFace->idVertices[2]]->idTriangle == triangleIndex) {
        this->vertices[currentFace->idVertices[2]]->idTriangle = lastFaceId + 1;
    }
    this->vertices[vertexIndex]->idTriangle = lastFaceId + 2;

    // Create or change 3 triangle
    this->faces.push_back(newFace1);
    this->faces.push_back(newFace2);
    currentFace->idVertices[2] = vertexIndex;
}

void Mesh::faceFlip(int triangleIndex1, int triangleIndex2) {
    Face *face1 = this->faces[triangleIndex1];
    Face *face2 = this->faces[triangleIndex2];

    // Find connect verticies
    unsigned int f1Index;
    unsigned int f2Index;
    for (unsigned int i = 0; i < 3; i++) {
        if (face1->idAdjFaces[i] == triangleIndex2)
            f1Index = i;

        if (face2->idAdjFaces[i] == triangleIndex1)
            f2Index = i;
    }

    // Flip faces
    int f1ChangeIndex1 = (f1Index == 2) ? 0 : f1Index + 1;
    int f2ChangeIndex1 = (f2Index == 2) ? 0 : f2Index + 1;
    int f1ChangeIndex2 = (f1Index == 0) ? 2 : f1Index - 1;
    int f2ChangeIndex2 = (f2Index == 0) ? 2 : f2Index - 1;

    if(this->vertices[face1->idVertices[f1ChangeIndex1]]->idTriangle == triangleIndex1)
        this->vertices[face1->idVertices[f1ChangeIndex1]]->idTriangle = triangleIndex2;

    if(this->vertices[face2->idVertices[f2ChangeIndex1]]->idTriangle == triangleIndex2)
        this->vertices[face2->idVertices[f2ChangeIndex1]]->idTriangle = triangleIndex1;

    face1->idVertices[f1ChangeIndex1] = face2->idVertices[f2Index];
    face2->idVertices[f2ChangeIndex1] = face1->idVertices[f1Index];

    // Catch neigh Faces
    int idNeighF1 = face1->idAdjFaces[f1ChangeIndex2];
    int idNeighF2 = face1->idAdjFaces[f1ChangeIndex1];
    int idNeighF3 = face2->idAdjFaces[f2ChangeIndex1];
    int idNeighF4 = face2->idAdjFaces[f2ChangeIndex2];

    // change flipedFace adjFaces
    face1->idAdjFaces[f1Index] = idNeighF4;
    face1->idAdjFaces[f1ChangeIndex1] = idNeighF2;
    face1->idAdjFaces[f1ChangeIndex2] = triangleIndex2;

    face2->idAdjFaces[f2Index] = idNeighF1;
    face2->idAdjFaces[f2ChangeIndex1] = idNeighF3;
    face2->idAdjFaces[f2ChangeIndex2] = triangleIndex1;

    // change neigh Face adjFaces
    for (int i = 0; i < 3; i++) {
        if (idNeighF4 != -1) {
            Face *neighFace4 = this->faces[idNeighF4];

            if (neighFace4->idAdjFaces[i] == triangleIndex2)
                neighFace4->idAdjFaces[i] = triangleIndex1;
        }

        if (idNeighF1 != -1) {
            Face *neighFace1 = this->faces[idNeighF1];

            if (neighFace1->idAdjFaces[i] == triangleIndex1)
                neighFace1->idAdjFaces[i] = triangleIndex2;
        }
    }
}

void Mesh::createInfiniteEdge() {
    int infVertId = 0;
    long long nbFace = this->faces.size();

    int nbInfTriangle = 0;
    for (unsigned int j = 0; j < nbFace; j++) {
        for (unsigned int i = 0; i < 3; i++) {
            Face* currentFace = this->faces[j];
            if(currentFace->idAdjFaces[i] == -1) {
                int vertice1 = (i == 2) ? 0 : i + 1;
                int vertice2 = (i == 0) ? 2 : i - 1;
                Face* newFace = new Face(infVertId, currentFace->idVertices[vertice2], currentFace->idVertices[vertice1]);
                this->faces.push_back(newFace);
                nbInfTriangle++;

                long long lastFaceId = this->faces.size() - 1;
                this->faces[lastFaceId]->idAdjFaces[0] = j;
                currentFace->idAdjFaces[i] = lastFaceId;
            }
        }
    }
    this->vertices[infVertId]->idTriangle = (int)this->faces.size() - 1;

    for (unsigned int i = nbFace; i < (nbFace + nbInfTriangle); i++) {
        Face* mainFace = this->faces[i];
        for (unsigned int j = nbFace; j < (nbFace + nbInfTriangle); j++) {
            Face* currentFace = this->faces[j];
            if(currentFace->idVertices[2] == mainFace->idVertices[1]) {
                currentFace->idAdjFaces[1] = i;
                mainFace->idAdjFaces[2] = j;
            }
        }
    }
}

void Mesh::insertOutside(int vertexIndex) {
    int infVertId = 0;
    Vertex *infiniteVert = this->vertices[0];

    unsigned int nbNewFace = 0;
    std::vector<int> needDelete;
    std::vector<int> needUpdate;

    Circulator_on_faces cof = this->incident_faces(infiniteVert);
    Circulator_on_faces cofBegin = cof;
    do {
        Face *currentTriangle = *cof;
        int indexP1 = currentTriangle->idVertices[1];
        int indexP2 = currentTriangle->idVertices[2];
        Vertex *p = this->vertices[vertexIndex];
        Vertex *p1 = this->vertices[indexP1];
        Vertex *p2 = this->vertices[indexP2];

        if (this->checkOrientation(p->point, p1->point, p2->point) > 0) {
            Face *newFace = new Face(vertexIndex, indexP1, indexP2);
            this->faces.push_back(newFace);
            long long newFaceId = this->faces.size() - 1;

            // newFace adj
            newFace->idAdjFaces[0] = currentTriangle->idAdjFaces[0];
            this->vertices[vertexIndex]->idTriangle = newFaceId;

            // not inf neighboors adj
            Face *neighFace = this->faces[newFace->idAdjFaces[0]];
            for (int i = 0; i < 3; i++) {
                if (neighFace->idAdjFaces[i] == cof.faceIndex)
                    neighFace->idAdjFaces[i] = newFaceId;
            }

            needUpdate.push_back(newFaceId);
            needDelete.push_back(cof.faceIndex);
            nbNewFace++;
        }

        ++cof;
    } while (cof != cofBegin);

    // delete some infinite face
    for (unsigned long long i = 0; i < needDelete.size(); i++) {
        for (int j = 0; j < 3; j++) {
            if (this->faces[needDelete[i]]->idAdjFaces[j] != -1) {
                Face* adjFace = this->faces[this->faces[needDelete[i]]->idAdjFaces[j]];
                for (int k = 0; k < 3; k++) {
                    if (adjFace->idAdjFaces[k] == needDelete[i]) {
                        adjFace->idAdjFaces[k] = -1;
                    }
                }
            }
        }
        this->faces.erase(this->faces.begin() + needDelete[i]);

        Iterator_on_faces itf;
        Iterator_on_faces itfEnd = this->faces_past_the_end();
        for (itf = this->faces_begin(); itf != itfEnd; ++itf) {
            for (int j = 0; j < 3; j++) {
                if ((*itf)->idAdjFaces[j] > needDelete[i]) {
                    (*itf)->idAdjFaces[j] -= 1;
                }
            }
        }

        Iterator_on_vertices itv;
        for (itv = this->vertices_begin(); itv != this->vertices_past_the_end(); ++itv) {
            if ((*itv)->idTriangle > needDelete[i]) {
                (*itv)->idTriangle -= 1;
            }
        }
    }

    // Refractor the tab
    for (unsigned long long i = 0; i < needUpdate.size(); i++) {
        needUpdate[i] -= (int)needDelete.size();
    }

    // New faces neighboors
    for (unsigned long long i = 0; i < needUpdate.size(); i++) {
        Face* mainFace = this->faces[needUpdate[i]];
        for (unsigned long long j = 0; j < needUpdate.size(); j++) {
            Face* currentFace = this->faces[needUpdate[j]];
            if(currentFace->idVertices[2] == mainFace->idVertices[1]) {
                currentFace->idAdjFaces[1] = needUpdate[i];
                mainFace->idAdjFaces[2] = needUpdate[j];
            }
        }
    }

    long long nbFace = this->faces.size();
    int nbNewInfTriangle = 0;

    // New infinite face creation
    for (unsigned long long i = 0; i < needUpdate.size(); i++) {
        Face* currentFace = this->faces[needUpdate[i]];
        for (int j = 0; j < 3; j++) {
            if(currentFace->idAdjFaces[j] == -1) {
                int vertice1 = (j == 2) ? 0 : j + 1;
                int vertice2 = (j == 0) ? 2 : j - 1;
                Face* newFace = new Face(infVertId, currentFace->idVertices[vertice2], currentFace->idVertices[vertice1]);
                this->faces.push_back(newFace);
                nbNewInfTriangle++;

                long long lastFaceId = this->faces.size() - 1;
                this->faces[lastFaceId]->idAdjFaces[0] = needUpdate[i];
                currentFace->idAdjFaces[j] = lastFaceId;
                infiniteVert->idTriangle = lastFaceId;
            }
        }
    }

    // Infinite face neighboors
    for (unsigned int i = nbFace; i < (nbFace + nbNewInfTriangle); i++) {
        Face* mainFace = this->faces[i];
        Iterator_on_faces itf;
        for (itf = this->faces_begin(); itf != this->faces_past_the_end(); ++itf) {
            if((*itf)->idVertices[2] == mainFace->idVertices[1]
               && (*itf)->idVertices[0] == infVertId) {
                (*itf)->idAdjFaces[1] = i;
                mainFace->idAdjFaces[2] = itf.currentIndex;
            }

            if((*itf)->idVertices[1] == mainFace->idVertices[2]
               && (*itf)->idVertices[0] == infVertId) {
                (*itf)->idAdjFaces[2] = i;
                mainFace->idAdjFaces[1] = itf.currentIndex;
            }
        }
    }
}

void Mesh::delaunay() {
    bool inTriangle;

    faces.push_back(new Face(1, 2, 3));
    vertices[1]->idTriangle = 0;
    vertices[2]->idTriangle = 0;
    vertices[3]->idTriangle = 0;
    createInfiniteEdge();

    for (unsigned long long vertexIndex = 4; vertexIndex < vertices.size(); vertexIndex ++) {
        inTriangle = false;
        Vertex* v = vertices[vertexIndex];

        std::map<int, int> flipEdges;

        for (unsigned long long faceIndex = 0; faceIndex < faces.size(); faceIndex ++) {
            Face* f = faces[faceIndex];

            if (f->idVertices[0] != 0 && f->idVertices[1] != 0 && f->idVertices[2] != 0) {
                Point* p1 = vertices[f->idVertices[0]]->point;
                Point* p2 = vertices[f->idVertices[1]]->point;
                Point* p3 = vertices[f->idVertices[2]]->point;

                if (checkInTriangle(v->point, p1, p2, p3)) {
                    faceSplit(faceIndex, vertexIndex);
                    v->idTriangle = faceIndex;

                    inTriangle = true;
                    break;
                }
            }
        }

        if (!inTriangle) {
            insertOutside(vertexIndex);
        }

        Circulator_on_faces cf = incident_faces(v);
        Circulator_on_faces cfBegin = cf;

        do {
            if ((*cf)->idVertices[0] != 0 && (*cf)->idVertices[1] != 0 && (*cf)->idVertices[2] != 0) {
                int oppositeTriangleIndex = -1;
                unsigned int index;

                if ((*cf)->idVertices[0] == (int)vertexIndex) {
                    oppositeTriangleIndex = (*cf)->idAdjFaces[0];
                    index = 0;
                } else if ((*cf)->idVertices[1] == (int)vertexIndex) {
                    oppositeTriangleIndex = (*cf)->idAdjFaces[1];
                    index = 1;
                } else if ((*cf)->idVertices[2] == (int)vertexIndex) {
                    oppositeTriangleIndex = (*cf)->idAdjFaces[2];
                    index = 2;
                }

                if (oppositeTriangleIndex != -1) {
                    Face* oppositeTriangle = faces[oppositeTriangleIndex];

                    if (oppositeTriangle->idVertices[0] != 0
                        && oppositeTriangle->idVertices[1] != 0
                        && oppositeTriangle->idVertices[2] != 0) {

                        int oppositeIndex = -1;

                        if ((*cf)->idVertices[(index + 1) % 3] == oppositeTriangle->idVertices[0]) {
                            oppositeIndex = 1;
                        } else if ((*cf)->idVertices[(index + 1) % 3] == oppositeTriangle->idVertices[1]) {
                            oppositeIndex = 2;
                        } else if ((*cf)->idVertices[(index + 1) % 3] == oppositeTriangle->idVertices[2]) {
                            oppositeIndex = 0;
                        }

                        if (oppositeIndex != -1) {
                            unsigned int oppositeVertex = oppositeTriangle->idVertices[oppositeIndex];

                            Point* p = vertices[oppositeVertex]->point;
                            Point* p1 = vertices[(*cf)->idVertices[0]]->point;
                            Point* p2 = vertices[(*cf)->idVertices[1]]->point;
                            Point* p3 = vertices[(*cf)->idVertices[2]]->point;

                            if (checkInCircle(p, p1, p2, p3) > 0) {
                                flipEdges.insert({cf.faceIndex, oppositeTriangleIndex});
                            }
                        }
                    }
                }
            }

            ++cf;
        } while (cf != cfBegin);

        std::map<int, int>::iterator it;

        for (it = flipEdges.begin(); it != flipEdges.end(); it++) {
            faceFlip(it->first, it->second);
        }
    }
}

void Mesh::voronoi() {
    Iterator_on_faces itf;
    for (itf = faces_begin(); itf != faces_past_the_end(); ++itf) {
        (*itf)->barycentre = getBarycenter(
            vertices[(*itf)->idVertices[0]]->point,
            vertices[(*itf)->idVertices[1]]->point,
            vertices[(*itf)->idVertices[2]]->point
        );
        (*itf)->radius = sqrt(
            pow((*itf)->barycentre->x - vertices[(*itf)->idVertices[0]]->point->x, 2) +
            pow((*itf)->barycentre->y - vertices[(*itf)->idVertices[0]]->point->y, 2)
        );
    }
}

void Mesh::setLaplacians() {
    Iterator_on_vertices itv;
    double* laplacianVector;
    for (itv = vertices_begin(); itv != vertices_past_the_end(); ++itv) {
        laplacianVector = laplacian(*itv);
        (*itv)->laplacian[0] = laplacianVector[0];
        (*itv)->laplacian[1] = laplacianVector[1];
        (*itv)->laplacian[2] = laplacianVector[2];
    }
}

void Mesh::crust() {
    for (Face* f : faces) {
        vertices.push_back(new Vertex(f->barycentre));
        delete f;
    }
    faces.clear();

    delaunay();
}


// Predicates
double Mesh::checkOrientation(Point* p1, Point* p2, Point* p3) {
    return (p1->x - p3->x) * (p2->y - p3->y) - (p2->x - p3->x) * (p1->y - p3->y);
}

double Mesh::checkInTriangle(Point* p, Point* p1, Point* p2, Point* p3) {
    double d1 = checkOrientation(p, p1, p2);
    double d2 = checkOrientation(p, p2, p3);
    double d3 = checkOrientation(p, p3, p1);

    bool has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
    bool has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

    return !(has_neg && has_pos);
}

double Mesh::checkInCircle(Point* p, Point* p1, Point* p2, Point* p3) {
    double axdx = p1->x - p->x;
    double aydy = p1->y - p->y;
    double ad = std::pow(axdx, 2) + std::pow(aydy, 2);

    double bxdx = p2->x - p->x;
    double bydy = p2->y - p->y;
    double bd = std::pow(bxdx, 2) + std::pow(bydy, 2);

    double cxdx = p3->x - p->x;
    double cydy = p3->y - p->y;
    double cd = std::pow(cxdx, 2) + std::pow(cydy, 2);

    double det1 = axdx * (bydy * cd - cydy * bd);
    double det2 = bxdx * (aydy * cd - cydy * ad);
    double det3 = cxdx * (aydy * bd - bydy * ad);

    return det1 - det2 + det3;
}


// Geometrical functions
double Mesh::getAire(Point* pA, Point* pB, Point* pC) {
    double dAB = std::sqrt(std::pow(pB->x - pA->x, 2) + std::pow(pB->y - pA->y, 2) + std::pow(pB->z - pA->z, 2));
    double dAC = std::sqrt(std::pow(pC->x - pA->x, 2) + std::pow(pC->y - pA->y, 2) + std::pow(pC->z - pA->z, 2));
    double dBC = std::sqrt(std::pow(pC->x - pB->x, 2) + std::pow(pC->y - pB->y, 2) + std::pow(pC->z - pB->z, 2));
    double halfP = (dAB + dAC + dBC) / 2.0;

    // Calcul de l'aire par la formule de Héron
    return std::sqrt(halfP * (halfP - dAB) * (halfP - dAC) * (halfP - dBC));
}

double Mesh::cotangent(Point* pA, Point* pB, Point* pC) {
    double vBA[3] = {
        pA->x - pB->x,
        pA->y - pB->y,
        pA->z - pB->z
    };

    double vBC[3] = {
        pC->x - pB->x,
        pC->y - pB->y,
        pC->z - pB->z
    };

    double vBALength = std::sqrt(std::pow(vBA[0], 2) + std::pow(vBA[1], 2) + std::pow(vBA[2], 2));
    vBA[0] = vBA[0] / vBALength;
    vBA[1] = vBA[1] / vBALength;
    vBA[2] = vBA[2] / vBALength;

    double vBCLength = std::sqrt(std::pow(vBC[0], 2) + std::pow(vBC[1], 2) + std::pow(vBC[2], 2));
    vBC[0] = vBC[0] / vBCLength;
    vBC[1] = vBC[1] / vBCLength;
    vBC[2] = vBC[2] / vBCLength;

    double dotProduct = vBA[0] * vBC[0] + vBA[1] * vBC[1] + vBA[2] * vBC[2];

    double crossProductX = vBA[1] * vBC[2] - vBA[2] * vBC[1];
    double crossProductY = vBA[2] * vBC[0] - vBA[0] * vBC[2];
    double crossProductZ = vBA[0] * vBC[1] - vBA[1] * vBC[0];
    double crossProductLength = std::sqrt(std::pow(crossProductX, 2) + std::pow(crossProductY, 2) + std::pow(crossProductZ, 2));

    // Calcul de l'angle
    return dotProduct / crossProductLength;
}

double Mesh::tangent(Point* pA, Point* pB, Point* pC) {
    double vBA[3] = {
        pA->x - pB->x,
        pA->y - pB->y,
        pA->z - pB->z
    };

    double vBC[3] = {
        pC->x - pB->x,
        pC->y - pB->y,
        pC->z - pB->z
    };

    double vBALength = std::sqrt(std::pow(vBA[0], 2) + std::pow(vBA[1], 2) + std::pow(vBA[2], 2));
    vBA[0] = vBA[0] / vBALength;
    vBA[1] = vBA[1] / vBALength;
    vBA[2] = vBA[2] / vBALength;

    double vBCLength = std::sqrt(std::pow(vBC[0], 2) + std::pow(vBC[1], 2) + std::pow(vBC[2], 2));
    vBC[0] = vBC[0] / vBCLength;
    vBC[1] = vBC[1] / vBCLength;
    vBC[2] = vBC[2] / vBCLength;

    double dotProduct = vBA[0] * vBC[0] + vBA[1] * vBC[1] + vBA[2] * vBC[2];

    double crossProductX = vBA[1] * vBC[2] - vBA[2] * vBC[1];
    double crossProductY = vBA[2] * vBC[0] - vBA[0] * vBC[2];
    double crossProductZ = vBA[0] * vBC[1] - vBA[1] * vBC[0];
    double crossProductLength = std::sqrt(std::pow(crossProductX, 2) + std::pow(crossProductY, 2) + std::pow(crossProductZ, 2));

    // Calcul de l'angle
    return crossProductLength / dotProduct;
}

double* Mesh::laplacian(Vertex* v) {
    double* laplacianVector = new double[3];
    laplacianVector[0] = 0.0;
    laplacianVector[1] = 0.0;
    laplacianVector[2] = 0.0;

    double aij, bij;
    double aire = 0.0;

    Circulator_on_faces cf = incident_faces(v);
    Circulator_on_faces cfBegin = cf;

    Circulator_on_vertices cv = incident_vertex(v);
    Circulator_on_vertices cvBegin = cv;
    Circulator_on_vertices cvDuFutur = cv;
    Circulator_on_vertices cvDuPasse = cv;
    ++cvDuFutur;
    ++cvDuPasse;
    ++cvDuPasse;

    do {
        aire += getAire(
                    vertices[(*cf)->idVertices[0]]->point,
                    vertices[(*cf)->idVertices[1]]->point,
                    vertices[(*cf)->idVertices[2]]->point
                );
        ++cf;
    } while (cf != cfBegin);

    do {
        aij = cotangent((*cv)->point, (*cvDuPasse)->point, v->point);
        bij = cotangent((*cv)->point, (*cvDuFutur)->point, v->point);

        laplacianVector[0] += (aij + bij) * ((*cv)->point->x - v->point->x);
        laplacianVector[1] += (aij + bij) * ((*cv)->point->y - v->point->y);
        laplacianVector[2] += (aij + bij) * ((*cv)->point->z - v->point->z);

        ++cvDuFutur;
        ++cvDuPasse;
        ++cv;
    } while (cv != cvBegin);

    laplacianVector[0] /= (2 / 3.f * aire);
    laplacianVector[1] /= (2 / 3.f * aire);
    laplacianVector[2] /= (2 / 3.f * aire);

    double norme = std::sqrt(std::pow(laplacianVector[0], 2) + std::pow(laplacianVector[1], 2) + std::pow(laplacianVector[2], 2));

    if (norme != 0.f) {
        laplacianVector[0] = std::abs(laplacianVector[0] / (norme));
        laplacianVector[1] = std::abs(laplacianVector[1] / (norme));
        laplacianVector[2] = std::abs(laplacianVector[2] / (norme));
    } else {
        laplacianVector[0] = std::abs(laplacianVector[0]);
        laplacianVector[1] = std::abs(laplacianVector[1]);
        laplacianVector[2] = std::abs(laplacianVector[2]);
    }

    return laplacianVector;
}

Point* Mesh::getBarycenter(Point* pA, Point* pB, Point* pC) {
    double tanA = tangent(pC, pA, pB);
    double tanB = tangent(pA, pB, pC);
    double tanC = tangent(pB, pC, pA);

    double norme = 2 * tanA + 2 * tanB + 2 * tanC;
    double tanAB = (tanA + tanB) / norme;
    double tanBC = (tanB + tanC) / norme;
    double tanCA = (tanC + tanA) / norme;

    Point* p = new Point();
    p->x = tanBC * pA->x + tanCA * pB->x + tanAB * pC->x;
    p->y = tanBC * pA->y + tanCA * pB->y + tanAB * pC->y;
    p->z = tanBC * pA->z + tanCA * pB->z + tanAB * pC->z;

    return p;
}



// Mesh constructor
Mesh::Mesh(const Mesh &mesh) {
    faces = mesh.faces;
    vertices = mesh.vertices;
}



// Face operators
bool Face::operator !=(const Face& f) const {
    return (idAdjFaces != f.idAdjFaces || idVertices != f.idVertices);
}

bool Face::operator ==(const Face& f) const {
    return !(*this != f);
}



// Mesh operators
bool Mesh::operator !=(const Mesh& mesh) const {
    return (faces != mesh.faces || vertices != mesh.vertices);
}



// Mesh functions for Iterator_on_faces
Iterator_on_faces Mesh::faces_begin() {
    Iterator_on_faces its;
    its.mesh = this;
    its.currentIndex = 0;
    return its;
}

Iterator_on_faces Mesh::faces_past_the_end() {
    Iterator_on_faces its;
    its.mesh = this;
    its.currentIndex = (int)faces.size();
    return its;
}



// Iterator_on_faces operators
Face* Iterator_on_faces::operator*() {
    return mesh->faces[currentIndex];
}

Iterator_on_faces& Iterator_on_faces::operator ++() {
    currentIndex ++;
    return *this;
}

bool Iterator_on_faces::operator !=(const Iterator_on_faces& fi) const {
    return (currentIndex != fi.currentIndex || mesh != fi.mesh);
}



// Mesh functions for Iterator_on_vertices
Iterator_on_vertices Mesh::vertices_begin() {
    Iterator_on_vertices its;
    its.mesh = this;
    its.currentIndex = 0;
    return its;
}

Iterator_on_vertices Mesh::vertices_past_the_end() {
    Iterator_on_vertices its;
    its.mesh = this;
    its.currentIndex = (int)vertices.size();
    return its;
}



// Iterator_on_faces operators
Vertex* Iterator_on_vertices::operator*() {
    return mesh->vertices[currentIndex];
}

Iterator_on_vertices& Iterator_on_vertices::operator ++() {
    currentIndex ++;
    return *this;
}

bool Iterator_on_vertices::operator !=(const Iterator_on_vertices& fi) const {
    return (currentIndex != fi.currentIndex || mesh != fi.mesh);
}



// Mesh functions for circulators
Circulator_on_faces Mesh::incident_faces(Vertex *v) {
    Circulator_on_faces cof;

    cof.mainVertex = v;
    cof.mesh = this;
    cof.faceIndex = v->idTriangle;

    return cof;
}

Circulator_on_vertices Mesh::incident_vertex(Vertex *v) {
    Circulator_on_vertices cov;

    cov.mainVertex = v;
    cov.mesh = this;
    cov.faceIndex = v->idTriangle;

    return cov;
}



// Circulator_on_faces operators
Circulator_on_faces& Circulator_on_faces::operator ++() {
    Face *currentFace = mesh->faces[faceIndex];
    int nextTriangleId = -1;

    // Take opposite triangle of current vertex
    for (int i = 0; i < 3; i++) {
        if (mesh->vertices[currentFace->idVertices[i]] == mainVertex) {
            nextTriangleId = (i + 1) % 3;
            break;
        }
    }

    faceIndex = currentFace->idAdjFaces[nextTriangleId];
    return *this;
}

Face* Circulator_on_faces::operator*() {
    return mesh->faces[faceIndex];
}

bool Circulator_on_faces::operator !=(const Circulator_on_faces& cof) const {
    return (faceIndex != cof.faceIndex || mainVertex != cof.mainVertex || mesh != cof.mesh);
}



// Circulator_on_vertices operators
Circulator_on_vertices& Circulator_on_vertices::operator ++() {
    Face *currentFace = mesh->faces[faceIndex];
    int nextTriangleId = -1;

    // Take opposite triangle of current vertex
    for (int i = 0; i < 3; i++) {
        if (mesh->vertices[currentFace->idVertices[i]] == mainVertex) {
            nextTriangleId = (i + 1) % 3;
            break;
        }
    }

    faceIndex = currentFace->idAdjFaces[nextTriangleId];
    return *this;
}

Vertex* Circulator_on_vertices::operator*() {
    Face *currentFace = mesh->faces[faceIndex];
    int vertexId = -1;

    // Take opposite triangle of current vertex
    for (int i = 0; i < 3; i++) {
        if (mesh->vertices[currentFace->idVertices[i]] == mainVertex) {
            vertexId = (i + 1) % 3;
            break;
        }
    }

    return mesh->vertices[currentFace->idVertices[vertexId]];
}

bool Circulator_on_vertices::operator !=(const Circulator_on_vertices& cov) const {
    return (faceIndex != cov.faceIndex || mainVertex != cov.mainVertex || mesh != cov.mesh);
}



// Mesh loading functions
void GeometricWorld::loadFile(const char* filename) {
    mesh = Mesh();

    // Create infinite vertex
    this->mesh.vertices.push_back(new Vertex(new Point(0, 0, -10)));

    std::ifstream file(filename);
    if (file.is_open()) {
        std::stringstream off;
        off << file.rdbuf();
        std::vector<Point> points;
        std::string line, value;
        std::getline(off, line);
        std::stringstream tmpfirstLine (line);
        std::vector<unsigned int> stats;
        while(getline(tmpfirstLine, value, ' ')) {
                stats.push_back(std::stoi(value));
        }

        mesh.nbVertices = stats[0];
        // Add all vertices loop
        for(unsigned int i = 0; i < stats[0]; i++) {
            std::vector<double> position;
            std::getline(off, line);
            std::stringstream tmpLine (line);
            while(getline(tmpLine, value, ' ')) {
                    position.push_back(std::stod(value));
            }
            mesh.vertices.push_back(new Vertex(new Point(position[0], position[1], position[2])));
        }

        if (stats[1] > 0) {
            // Add all faces loop
            for(unsigned int i = stats[0]; i < stats[1] + stats[0]; i++) {
                std::vector<unsigned int> position;
                std::getline(off, line);
                std::stringstream tmpLine (line);
                while(getline(tmpLine, value, ' ')) {
                        position.push_back(std::stoi(value));
                }

                mesh.faces.push_back(new Face(position[1] + 1, position[2] + 1, position[3] + 1));

                // Add indicent triangle index loop
                for (int j = 1; j <= 3; j++) {
                    if (mesh.vertices[position[j] + 1]->idTriangle == -1) {
                        mesh.vertices[position[j] + 1]->idTriangle = i - stats[0];
                    }
                }
            }
            file.close();

            // Add all adjacent triangles loop
            std::map<std::pair<int, int>, int> adjMap;
            for (unsigned int i = 0; i < mesh.faces.size(); i++) {
                for (int j = 0; j < 3; j++) {
                    int vertex1 = mesh.faces[i]->idVertices[j];
                    int vertex2 = mesh.faces[i]->idVertices[(j + 1) % 3];
                    std::pair<int, int> arete = {vertex1, vertex2};
                    auto it = adjMap.find(arete);

                    if (it == adjMap.end()) {
                        arete = {vertex2, vertex1};
                        it = adjMap.find(arete);
                        if (it == adjMap.end()) {
                            // Edge is not in adjMap -> insert it
                            adjMap.insert({arete, i});
                        }
                    } else {
                        // Edge is in adjMap -> add value to table
                        mesh.faces[i]->idAdjFaces[(j + 2) % 3] = it->second;

                        // Test all cases for the opposite triangle in the other triangle
                        if ((it->first).first == mesh.faces[it->second]->idVertices[0]) {
                            if ((it->first).second == mesh.faces[it->second]->idVertices[1]) {
                                mesh.faces[it->second]->idAdjFaces[2] = i;
                            } else {
                                mesh.faces[it->second]->idAdjFaces[1] = i;
                            }
                        } else if ((it->first).second == mesh.faces[it->second]->idVertices[0]) {
                            if ((it->first).first == mesh.faces[it->second]->idVertices[1]) {
                                mesh.faces[it->second]->idAdjFaces[2] = i;
                            } else {
                                mesh.faces[it->second]->idAdjFaces[1] = i;
                            }
                        } else {
                            mesh.faces[it->second]->idAdjFaces[0] = i;
                        }
                    }
                }
            }
        } else {
            mesh.delaunay();
            mesh.setLaplacians();
            mesh.voronoi();

            if (dm == 5) {
                mesh.crust();
            }
        }
    } else {
        std::cout << "Unable to open file";
    }
}

GeometricWorld::GeometricWorld(const char* filename) {
    loadFile(filename);
}



// Draw functions
void GeometricWorld::draw() {
    switch(dm) {
        case 1: drawPlain(); break;
        case 2: drawWireframe(); break;
        case 3: drawLaplacian(); break;
        case 4: drawVoronoi(); break;
        case 5: drawCrust(); break;
        default: drawWireframe(); break;
    }
}

void glPointDraw(const Point & p) {
    glVertex3f(p.x, p.y, p.z);
}

void GeometricWorld::drawPlain() {
    Iterator_on_faces itf;
    for (itf = mesh.faces_begin(); itf != mesh.faces_past_the_end(); ++itf) {
        Vertex* v1 = mesh.vertices[(*itf)->idVertices[0]];
        Vertex* v2 = mesh.vertices[(*itf)->idVertices[1]];
        Vertex* v3 = mesh.vertices[(*itf)->idVertices[2]];

        glColor3d(1, 1, 1);
        glBegin(GL_TRIANGLES);
        glPointDraw(*v1->point);
        glPointDraw(*v2->point);
        glPointDraw(*v3->point);
        glEnd();
    }
}

void GeometricWorld::drawWireframe() {
    Iterator_on_faces itf;
    for (itf = mesh.faces_begin(); itf != mesh.faces_past_the_end(); ++itf) {
        Vertex* v1 = mesh.vertices[(*itf)->idVertices[0]];
        Vertex* v2 = mesh.vertices[(*itf)->idVertices[1]];
        Vertex* v3 = mesh.vertices[(*itf)->idVertices[2]];

        if ((*itf)->idVertices[0] == 0 || (*itf)->idVertices[1] == 0 || (*itf)->idVertices[2] == 0) {
            glColor3d(0.5, 0.5, 0.5);
        } else {
            glColor3d(1, 1, 1);
        }
        glBegin(GL_LINE_STRIP);
        glPointDraw(*v1->point);
        glPointDraw(*v2->point);
        glEnd();
        glBegin(GL_LINE_STRIP);
        glPointDraw(*v1->point);
        glPointDraw(*v3->point);
        glEnd();
        glBegin(GL_LINE_STRIP);
        glPointDraw(*v2->point);
        glPointDraw(*v3->point);
        glEnd();
    }
}

void GeometricWorld::drawLaplacian() {
    Iterator_on_faces itf;
    for (itf = mesh.faces_begin(); itf != mesh.faces_past_the_end(); ++itf) {
        Vertex* v1 = mesh.vertices[(*itf)->idVertices[0]];
        Vertex* v2 = mesh.vertices[(*itf)->idVertices[1]];
        Vertex* v3 = mesh.vertices[(*itf)->idVertices[2]];

        glBegin(GL_LINE_STRIP);
        glColor3d(v1->laplacian[0], v1->laplacian[1], v1->laplacian[2]);
        glPointDraw(*v1->point);
        glColor3d(v2->laplacian[0], v2->laplacian[1], v2->laplacian[2]);
        glPointDraw(*v2->point);
        glEnd();
        glBegin(GL_LINE_STRIP);
        glColor3d(v1->laplacian[0], v1->laplacian[1], v1->laplacian[2]);
        glPointDraw(*v1->point);
        glColor3d(v3->laplacian[0], v3->laplacian[1], v3->laplacian[2]);
        glPointDraw(*v3->point);
        glEnd();
        glBegin(GL_LINE_STRIP);
        glColor3d(v2->laplacian[0], v2->laplacian[1], v2->laplacian[2]);
        glPointDraw(*v2->point);
        glColor3d(v3->laplacian[0], v3->laplacian[1], v3->laplacian[2]);
        glPointDraw(*v3->point);
        glEnd();
    }
}

void GeometricWorld::drawVoronoi() {
    Iterator_on_faces itf;
    for (itf = mesh.faces_begin(); itf != mesh.faces_past_the_end(); ++itf) {
        Vertex* v1 = mesh.vertices[(*itf)->idVertices[0]];
        Vertex* v2 = mesh.vertices[(*itf)->idVertices[1]];
        Vertex* v3 = mesh.vertices[(*itf)->idVertices[2]];

        if ((*itf)->idVertices[0] != 0 && (*itf)->idVertices[1] != 0 && (*itf)->idVertices[2] != 0) {
            glColor3d(1, 1, 1);
            glBegin(GL_LINE_STRIP);
            glPointDraw(*v1->point);
            glPointDraw(*v2->point);
            glEnd();

            glBegin(GL_LINE_STRIP);
            glPointDraw(*v1->point);
            glPointDraw(*v3->point);
            glEnd();

            glBegin(GL_LINE_STRIP);
            glPointDraw(*v2->point);
            glPointDraw(*v3->point);
            glEnd();

            glColor3d(0, 1, 0);
            glPointSize(5.0f);
            glBegin(GL_POINTS);
            glPointDraw(*(*itf)->barycentre);
            glEnd();
        } else {
            glColor3d(0.5, 0.5, 0.5);
            glBegin(GL_LINE_STRIP);
            glPointDraw(*v1->point);
            glPointDraw(*v2->point);
            glEnd();

            glBegin(GL_LINE_STRIP);
            glPointDraw(*v1->point);
            glPointDraw(*v3->point);
            glEnd();

            glBegin(GL_LINE_STRIP);
            glPointDraw(*v2->point);
            glPointDraw(*v3->point);
            glEnd();
        }
    }
}

void GeometricWorld::drawCrust() {
    Iterator_on_faces itf;
    for (itf = mesh.faces_begin(); itf != mesh.faces_past_the_end(); ++itf) {
        Vertex* v1 = mesh.vertices[(*itf)->idVertices[0]];
        Vertex* v2 = mesh.vertices[(*itf)->idVertices[1]];
        Vertex* v3 = mesh.vertices[(*itf)->idVertices[2]];

        if ((*itf)->idVertices[0] > 3 && (*itf)->idVertices[1] > 3 &&
            (*itf)->idVertices[0] <= mesh.nbVertices && (*itf)->idVertices[1] <= mesh.nbVertices) {
            glColor3d(1, 0, 0);
        } else {
            glColor3d(0, 0, 0.7);
        }

        glBegin(GL_LINE_STRIP);
        glPointDraw(*v1->point);
        glPointDraw(*v2->point);
        glEnd();

        if ((*itf)->idVertices[0] > 3 && (*itf)->idVertices[2] > 3 &&
            (*itf)->idVertices[0] <= mesh.nbVertices && (*itf)->idVertices[2] <= mesh.nbVertices) {
            glColor3d(1, 0, 0);
        } else {
            glColor3d(0, 0, 0.7);
        }

        glBegin(GL_LINE_STRIP);
        glPointDraw(*v1->point);
        glPointDraw(*v3->point);
        glEnd();

        if ((*itf)->idVertices[1] > 3 && (*itf)->idVertices[2] > 3 &&
            (*itf)->idVertices[1] <= mesh.nbVertices && (*itf)->idVertices[2] <= mesh.nbVertices) {
            glColor3d(1, 0, 0);
        } else {
            glColor3d(0, 0, 0.7);
        }

        glBegin(GL_LINE_STRIP);
        glPointDraw(*v2->point);
        glPointDraw(*v3->point);
        glEnd();
    }
}
