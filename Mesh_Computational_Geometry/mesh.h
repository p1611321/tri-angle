#ifndef MESH_H
#define MESH_H


#include <QGLWidget>
#include <QtMath>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <cmath>


class Point {
public:
    double x;
    double y;
    double z;

    Point(): x(), y(), z() {}
    Point(double _x, double _y, double _z): x(_x), y(_y), z(_z) {}
    Point(const Point& p): x(p.x), y(p.y), z(p.z) {}
};


class Vertex {
public:
    Point* point;
    int idTriangle;
    double* laplacian;

    Vertex(Point* _point) : point(_point), idTriangle(-1) {
        laplacian = new double[3];
        laplacian[0] = 1;
        laplacian[1] = 1;
        laplacian[2] = 1;
    }

    ~Vertex() {
        delete laplacian;
    }
};


class Face {
public:
    int* idVertices;
    int* idAdjFaces;
    Point* barycentre;
    double radius;

    Face(int v1, int v2, int v3) {
        idVertices = new int[3];
        idVertices[0] = v1;
        idVertices[1] = v2;
        idVertices[2] = v3;

        idAdjFaces = new int[3];
        idAdjFaces[0] = -1;
        idAdjFaces[1] = -1;
        idAdjFaces[2] = -1;

        barycentre = nullptr;
        radius = 0.0;
    }

    ~Face() {
        delete idVertices;
        delete idAdjFaces;
    }

    bool operator !=(const Face& f) const;
    bool operator ==(const Face& f) const;
};


class Mesh;


class Iterator_on_faces {
public:
    Mesh* mesh;
    unsigned int currentIndex;

    Face* operator*();
    Iterator_on_faces& operator++();
    bool operator!=(const Iterator_on_faces& fi) const;
};


class Iterator_on_vertices {
public:
    Mesh* mesh;
    unsigned int currentIndex;

    Vertex* operator*();
    Iterator_on_vertices& operator++();
    bool operator!=(const Iterator_on_vertices& vi) const;
};


class Circulator_on_faces {
public:
    Mesh* mesh;
    Vertex *mainVertex;
    int faceIndex;

    Face* operator*();
    Circulator_on_faces& operator++();
    bool operator!=(const Circulator_on_faces& vi) const;
};


class Circulator_on_vertices {
public:
    Mesh* mesh;
    Vertex *mainVertex;
    int faceIndex;

    Vertex* operator*();
    Circulator_on_vertices& operator++();
    bool operator!=(const Circulator_on_vertices& vi) const;
};


class Mesh {
public:
    // Vector of vertices
    std::vector<Vertex*> vertices;

    // Vector of faces
    std::vector<Face*> faces;

    int nbVertices;

    // Constructors automatically called to initialize a Mesh (default strategy)
    Mesh(): vertices(),faces(), nbVertices(0) {}
    Mesh(const Mesh &mesh);

    // Destructor automatically called before a Mesh is destroyed (default strategy)
    ~Mesh() {
        for (Vertex* v : vertices) {
            delete v;
        }
        vertices.clear();

        for (Face* f : faces) {
            delete f;
        }
        faces.clear();
    }

    // Iterators functions
    bool operator!=(const Mesh& fi) const;
    Iterator_on_faces faces_begin();
    Iterator_on_faces faces_past_the_end();
    Iterator_on_vertices vertices_begin();
    Iterator_on_vertices vertices_past_the_end();

    // Circulator functions
    Circulator_on_faces incident_faces(Vertex* v);
    Circulator_on_vertices incident_vertex(Vertex* v);

    // Geometry functions
    double getAire(Point* pA, Point* pB, Point* pC);
    double tangent(Point* pA, Point* pB, Point* pC);
    double cotangent(Point* pA, Point* pB, Point* pC);
    double* laplacian(Vertex* v);
    void setLaplacians();
    Point* getBarycenter(Point* pA, Point* pB, Point* pC);

    // Triangulation functions
    void faceSplit(int triangleIndex, int vertexIndex);
    void faceFlip(int triangleIndex1, int triangleIndex2);
    void insertOutside(int vertexIndex);
    void delaunay();
    void voronoi();
    void crust();

    // Infinite edge
    void createInfiniteEdge();

    // Predicates
    double checkOrientation(Point* p1, Point* p2, Point* p3);
    double checkInTriangle(Point* p, Point* p1, Point* p2, Point* p3);
    double checkInCircle(Point* p, Point* p1, Point* p2, Point* p3);
};


class GeometricWorld //Generally used to create a singleton instance
{
public:
  GeometricWorld(const char* _filename);

  void loadFile(const char* filename);

  void draw();
  void drawPlain();
  void drawWireframe();
  void drawLaplacian();
  void drawVoronoi();
  void drawCrust();

  Mesh mesh;
  char* filename;
  unsigned int dm;
};

#endif // MESH_H
